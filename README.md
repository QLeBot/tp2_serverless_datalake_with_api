# tp2_serverless_datalake_with_api

## Rendu de TP

Globalement le TP s'est plutôt bien passé, nous sommes restés longtemps sur la partie Terraform à cause de problèmes avec les noms de variables entre les différents fichiers s3, ec2 et lambda.

Pour le reste des parties, la 3.5 était assez perturbante à cause du manque d'indications.

Nous avons également eu un petit souci où l'instance EC2 ne voulait pas se créer cela à cause d'une simple erreur de région entre eu-west-3 et eu-west-1. L'erreur a donc été facile à résoudre

A noter qu'il est possible qu'il y ait une erreur dans la définition de S3, Terraform ne retourne pas d'erreur mais la création du bucket et de la fonction Lambda prend énormement de temps. En cherchant sur internet, nous n'avons pas pu trouver de solution concrète.