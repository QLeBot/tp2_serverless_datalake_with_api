# TODO : Create the lambda role with aws_iam_role
resource "aws_iam_role" "lambda_job_offer_role" {
  name = "lambda_job_offer_role"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

# TODO : Create lambda function with aws_lambda_function
resource "aws_lambda_function" "lambda_job_offer_function" {
  function_name = "lambda_job_offer_function"
  role          = aws_iam_role.lambda_job_offer_role.arn
  handler       = "lambda_main_app.lambda_handler"
  runtime       = "python3.7"
  filename      = "empty_lambda_code.zip"
  environment {
    variables = {
      BUCKET_NAME = aws_s3_bucket.job-offer-bucket-tp-esme.id
    }
  }
}

# TODO : Create a aws_iam_policy for the logging, this resource policy will be attached to the lambda role
resource "aws_iam_policy" "lambda_job_offer_policy" {
  name        = "lambda_job_offer_policy"
  description = "lambda_job_offer_policy"
  policy      = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Resource": "arn:aws:logs:*:*:*",
      "Effect": "Allow"
    }
  ]
}
EOF
}

# TODO : Attach the logging policy to the lamda role with aws_iam_role_policy_attachment
resource "aws_iam_role_policy_attachment" "lambda_job_offer_policy_attachment" {
  role       = aws_iam_role.lambda_job_offer_role.name
  policy_arn = aws_iam_policy.lambda_job_offer_policy.arn
}


# TODO : Attach the AmazonS3FullAccess policy to the lambda role with aws_iam_role_policy_attachment
resource "aws_iam_role_policy_attachment" "lambda_job_offer_s3_policy_attachment" {
  role       = aws_iam_role.lambda_job_offer_role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}


# TODO : Allow the lambda to be triggered by a s3 event with aws_lambda_permission
resource "aws_lambda_permission" "lambda_job_offer_permission" {
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.lambda_job_offer_function.function_name
  principal     = "s3.amazonaws.com"
  source_arn    = aws_s3_bucket.job-offer-bucket-tp-esme.arn
}
