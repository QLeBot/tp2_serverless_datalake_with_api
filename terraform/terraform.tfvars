# Lambda
data_processing_lambda_lambda_name = "data_processing_esme"
# S3
s3_user_bucket_name = "job-offers-bucket-tp-esme"
# Athena
processed_job_offers_key_name  = "job_offers/processed"
athena_results_key_bucket_name = "athena_results"
athena_db_name                 = "esme"
# Ec2
aws_public_key_ssh_path  = "~/.ssh/cloudDevOps_TP1_ssh.pub"
aws_private_key_ssh_path = "~/.ssh/cloudDevOps_TP1_ssh"
