# TODO : Create a s3 bucket with aws_s3_bucket

variable "id_group_tp" {
    type = string
}

resource "aws_s3_bucket" "job-offer-bucket-tp-esme" {
  #bucket = "job-offers-bucket-tp-esme-${var.id_group_tp}"
  bucket = "job-offer-bucket-tp-esme"
  acl    = "private"
  force_destroy = true
  versioning {
    enabled = false
  }
  tags = {
    Name = "job-offers-bucket-tp-esme-${var.id_group_tp}"
  }
}

# TODO : Create 1 nested folder :  job_offers/raw/  |  with  aws_s3_object
resource "aws_s3_object" "job-offers" {
  bucket = aws_s3_bucket.job-offer-bucket-tp-esme.id
  key    = "job_offers/raw/"
  source = "/dev/null"
  #etag   = filemd5("job_offers/raw/")
}


# TODO : Create an event to trigger the lambda when a file is uploaded into s3 with aws_s3_bucket_notification
resource "aws_s3_bucket_notification" "s3_job_offer_bucket_notification" {
  bucket = aws_s3_bucket.job-offer-bucket-tp-esme.id

  lambda_function {
    lambda_function_arn = aws_lambda_function.lambda_job_offer_function.arn
    events              = ["s3:ObjectCreated:*"]
    filter_prefix       = "job_offers/raw/"
    filter_suffix       = ".csv"
  }
}
