# PROVIDER

provider "aws" {
  region  = var.region
  s3_use_path_style = true
}
